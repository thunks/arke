package router

import "testing"

func TestEndpoints_NewEndpoints(t *testing.T) {
	tests := []struct {
		file        string
		path        string
		method      string
		status      int
		body        string
		description string
	}{
		{"testdata/endpoints_test.json", "/index", "GET", 200, "{ 'payload': 'hello world'}", "Simple GET"},
		{"testdata/endpoints_test.json", "another", "POST", 200, "{ 'payload': 'created'}", "Simple POST"},
	}

	for i, test := range tests {
		endpoints := NewEndpoints(test.file)
		endpoint := endpoints.Endpoints[i]

		if got := endpoint.Method; got != test.method {
			t.Errorf("Endpoint.Method: expected '%s' but got '%s'", test.method, got)
		}

		if got := endpoint.Status; got != test.status {
			t.Errorf("Endpoint.Status: expected '%d' but got %d'", test.status, got)
		}

		if got := endpoint.Body; got != test.body {
			t.Errorf("Endpoint.Body: expected '%s' but got '%s'", test.body, got)
		}

		if got := endpoint.Description; got != test.description {
			t.Errorf("Endpoint.Descrtiption: expected '%s' but got '%s'", test.description, got)
		}
	}
}
