package router

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type Endpoint struct {
	Path        string `json:"path"`
	Method      string `json:"method"`
	Status      int    `json:"status"`
	Body        string `json:"body"`
	Description string `json:"description"`
}

func (e *Endpoint) setDefaults() {
	if e.Status == 0 {
		e.Status = 200
	}
}

type Endpoints struct {
	Endpoints []Endpoint `json:"endpoints"`
}

func NewEndpoints(file string) Endpoints {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}

	var endpoints Endpoints
	err = json.Unmarshal(data, &endpoints)
	if err != nil {
		log.Fatal(err)
	}

	for i := range endpoints.Endpoints {
		endpoints.Endpoints[i].setDefaults()
	}

	return endpoints
}
