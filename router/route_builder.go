package router

import (
	"errors"
	"log"
	"net/http"
)

type HTTPMethods interface {
	Get(string, http.HandlerFunc)
	Delete(string, http.HandlerFunc)
	Post(string, http.HandlerFunc)
	Put(string, http.HandlerFunc)
	Patch(string, http.HandlerFunc)
}

type RouteBuilder struct {
	router    HTTPMethods
	endpoints Endpoints
}

func NewRouteBuilder(file string, router HTTPMethods) RouteBuilder {
	endpoints := NewEndpoints(file)
	return RouteBuilder{router, endpoints}
}

func (rb *RouteBuilder) Build() {
	for i := range rb.endpoints.Endpoints {
		endpoint := rb.endpoints.Endpoints[i]

		methodFunc, err := rb.findMethodFunc(endpoint.Method)
		if err != nil {
			log.Fatal(err)
		}

		methodFunc(endpoint.Path, func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(endpoint.Status)
			w.Write([]byte(endpoint.Body))
		})
	}
}

func (rb *RouteBuilder) findMethodFunc(method string) (func(string, http.HandlerFunc), error) {
	var methodFunc func(string, http.HandlerFunc)

	switch method {
	case "GET":
		methodFunc = rb.router.Get
	case "POST":
		methodFunc = rb.router.Post
	case "DELETE":
		methodFunc = rb.router.Delete
	case "PUT":
		methodFunc = rb.router.Put
	case "PATCH":
		methodFunc = rb.router.Patch
	default:
		methodFunc = nil
	}

	if methodFunc != nil {
		return methodFunc, nil
	}

	return nil, errors.New("Method not found in router")
}
