package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi"

	"gitlab.com/thunks/arke/config"
	"gitlab.com/thunks/arke/router"
)

func main() {
	args := os.Args
	config := config.NewConfig()
	config.Parse(args[0], args[1:])

	r := chi.NewRouter()
	builder := router.NewRouteBuilder(config.File, r)
	builder.Build()

	fmt.Printf("Starting server on port %d\n", config.Port)
	http.ListenAndServe(fmt.Sprintf(":%d", config.Port), r)
}
