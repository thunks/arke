package config

import "flag"

type Config struct {
	File string
	Port int
}

func NewConfig() Config {
	return Config{}
}

func (c *Config) Parse(progname string, args []string) {
	flags := flag.NewFlagSet(progname, flag.ContinueOnError)
	flags.StringVar(&c.File, "file", "arke.json", "Arke configuration file")
	flags.IntVar(&c.Port, "port", 3131, "Port to listen on")

	flags.Parse(args)
}
