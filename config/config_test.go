package config

import "testing"

func TestConfig_Parse(t *testing.T) {
	tests := []struct {
		args []string
		file string
		port int
	}{
		{[]string{"arke", "-file", "user-file.json", "-port", "1313"}, "user-file.json", 1313},
		{[]string{"arke"}, "arke.json", 3131},
	}

	for _, test := range tests {
		config := NewConfig()
		config.Parse(test.args[0], test.args[1:])

		if got := config.File; got != test.file {
			t.Errorf("Config.File: expected '%s' but got %s", test.file, got)
		}

		if got := config.Port; got != test.port {
			t.Errorf("Config.Port: expected %d but got %d", test.port, got)
		}
	}
}
