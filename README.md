# Arke

Arke is a service to easily mock third party APIs. Its purpose is to speed up development
and testing by serving defined paths with canned responses. It is not supposed to completely
replace running against the live API.

## Arkefile

A arkefile is simply a JSON file that describes the routes and canned responses Arke should
use.

```json
{
  "endpoints": [
    {
      "path": "/some/path",
      "method": "GET",
      "status": 200,
      "description": "Something about /some/path",
      "body": "Payload to be returned"
    },
    ...
  ]
}
```

## Running Arke

```bash
arke -file <arke.json> -port <3131>
```

Running Arke with no options will default to the following options:

| Option | Default   |
| ---    | ---       |
| -file  | arke.json |
| -port  | 3131      |
